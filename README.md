# cad_rc

Miscellaneous RC CAD parts.

## Info

Depends on MCAD.

This repo uses submodules, so be sure to clone appropriately.

## List of parts

- shock
  - `shock_clip_tamiyacva` -- shock pre-load clip for common Tamiya CVA oil-filled shocks
- Tamiya Rising Fighter
  - `risingfighter_rearshockmount` -- extends the rear shocks' mounting point further back, allowing the chassis to sit lower
  - `risingfighter_headlamp` -- headlamps that can be mounted to its body
- Tamiya TL-01
  - `tl01_arm_upper_front` -- upper front suspension arm
  - `tl01_arm_upper_rear` -- upper rear suspension arm
  - `tl01_batteryholder_minus5` -- battery holder with -5mm offset
  - `tl01_batteryholder_plus5` -- battery holder with +5mm offset
  - `tl01_bodymount_front` -- front body posts
  - `tl01_bodymount_rear` -- rear body posts
  - `tl01_dustcover` -- cover for the MSC resistor hole in the chassis
  - `tl01_gearbox_cover` -- cover for the hole in the rear gearbox
  - `tl01_shockmount_front` -- bracket that supports the front shock mounts
  - `tl01_shockmount_rear` -- bracket that supports the rear shock mounts
- tool
  - `tool_brushlessrotortool` -- a tool for inserting/removing 12mm brushless motor rotors
  - `tool_hextool` -- a tool for mounting hex wheels onto handheld drills
- wheel
  - `wheel_dish_hsp06010_front` -- a hex-mounted wheel for HSP 06010 (front) tires
  - `wheel_dish_hsp06010_front_bearing` -- a bearing-mounted wheel for HSP 06010 (front) tires
  - `wheel_dish_hsp06026_rear` -- a hex-mounted wheel for HSP 06026 (rear) tires
  - `wheel_dish_tamiya_astraldish_rear` -- a dish-style wheel that's the same size as a Tamiya Astral Dish rear
  - `wheel_dish_tamiya_holidaybuggy_rear_dt03` -- a dish-style wheel that allows a Tamiya Holiday Buggy rear tire to fit on a DT-03

