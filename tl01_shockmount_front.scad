use <lib/tl01_shockmount.scad>

module tl01_shockmount_front(width=80, offset_z=2.5, thickness=4) {
    tl01_shockmount(type="front", width=width, offset_z=offset_z, thickness=thickness);
}
$fn=32; tl01_shockmount_front();