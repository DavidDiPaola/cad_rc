use <lib/tl01_shockmount.scad>

module tl01_shockmount_rear(width=80, offset_z=2.5, thickness=4) {
    tl01_shockmount(type="rear", width=width, offset_z=offset_z, thickness=thickness);
}
$fn=32; tl01_shockmount_rear();