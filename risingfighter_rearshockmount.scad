/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

include <lib/scad_misc/alot.scad>

include <lib/scad_misc/oval_fs.scad>

module risingfighter_rearshockmount(shock_offset=5, margin_hole=+0.5) {
    module m3_hole_sub() {
        cylinder(d=2.8, h=alot); 
    }
    
    module shock_screw_hole_add() {
        // smooth 4mm OD part above threads
        cylinder(d=7.5, h=5.5);
        
        // threaded part
        mirror([0,0,1]) cylinder(d=7.5, h=8);
    }
    module shock_screw_hole_sub() {
        // smooth 4mm OD part above threads
        cylinder(d=4+margin_hole, h=alot);
        
        // threaded part
        mirror([0,0,1]) m3_hole_sub();
    }
    
    bump_d = 6 + margin_hole;
    bump_h = 3;
    module bump_sub() {
        cylinder(d=bump_d, h=bump_h);
    }
    
    arm = [bump_d+4, 45, bump_h+2];
    
    hole0_trans_xy = [0, arm.x/2];
    hole0_trans_z = [0, 0, shock_offset];
    hole2_trans = [0, arm.y];
    hole1_trans = [0, hole2_trans.y-18];
    difference() {
        union() {
            oval_fs(arm, y_is_center_to_center=true, center_x=true);
            
            translate(hole0_trans_xy) {
                translate(hole0_trans_z) shock_screw_hole_add();
            }
        }
        
        translate(hole0_trans_xy) {
            translate(hole0_trans_z) shock_screw_hole_sub();
            
            translate(hole1_trans) {
                cylinder(d=bump_d, h=bump_h);
                m3_hole_sub();
            }
            
            translate(hole2_trans) {
                m3_hole_sub();
            }
        }
    }
}

$fn=32;
risingfighter_rearshockmount();
