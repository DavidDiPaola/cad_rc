use <lib/bodypost_base_square.scad>

/*
A replacement for Tamiya 50737 part C20.
    post_h -- the height of the body post
    body_hole_spacing -- the center-to-center spacing of the body holes

FDM print settings:
    Build plate adhesion: brim

Post-print:
    1. Drill out the chassis mounting holes with a 3.2mm or 1/8in drill bit.
    2. Drill out the desired body mounting holes with a 1.5mm drill bit.
*/
module tl01_bodymount_rear(
    post_h=70, body_hole_spacing=65,
    square_width_tweak=+0.5, post_d_tweak=-0.2, post_hole_d_tweak=+0.5
) {
    chassis_mount_width = 64;
    post_offset_x = -(body_hole_spacing-chassis_mount_width) / 2;
    
    module left() {
        rotate([0, -90, -90]) {
            bodypost_base_square(
                square_width=5.75+square_width_tweak,
                post_offset_x=post_offset_x, post_offset_y=0,
                post_d=6+post_d_tweak, post_h=post_h,
                post_hole_d=1+post_hole_d_tweak
            );
        }
    }
    
    left();
    translate([12, 0]) mirror([1,0]) left();
}
tl01_bodymount_rear($fn=32);