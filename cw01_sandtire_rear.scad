use <lib/scad_misc/tube.scad>

use <lib/scad_misc/cube2.scad>

include <lib/scad_misc/alot.scad>

include <lib/cw01_rearwheelmount.scad>

/*
A sand tire for Tamiya CW-01 chassis cars.

FDM print settings:
    - Supports: Yes
    - Infill: >=20% (I used cubic infill)
    - Wall thickness: >=0.8mm (2 * 0.4mm nozzle diam.)
    - Top thickness: >=1.12mm (4 * 0.28mm layer height)
    - Bottom thickness: >=1.12mm (4 * 0.28mm layer height)

Post-print:
    1. Drill out the axle hole with a 5mm drill bit.
*/
module cw01_sandtire_rear(side="left", fin_angle=20, width=70, offset_=25, rim_thickness=2.5, face_thickness=4) {
    _fin_angle = (side=="left" ? -fin_angle : fin_angle);
    
    rim_od = 110;
    rim_id = rim_od - (rim_thickness*2);
    rim_h  = width;
    
    offset_absolute = [0, 0, (rim_h/2)-offset_];
    
    module outer() {
        module fin_half() {
            rotate([0, _fin_angle]) cube2([3, 20, rim_h/2], center_x=true);
        }
        //fin_half();
        
        difference() {
            union() {
                tube(od=rim_od, id=rim_id, h=rim_h);
                
                translate([0, 0, rim_h/2]) {
                    for (a=[0 : 360/10 : 360]) {
                        rotate([0, 0, a]) translate([0, (rim_od/2)-4, 0]) {
                            fin_half();
                            mirror([0,0,1]) fin_half();
                        }
                    }
                }
            }
            
            cylinder(d=rim_id, h=alot);
        }
    }
    //outer();
    
    module face() {
        module support() {
            tube(od=rim_id, id1=rim_id-5, id2=rim_id, h=5);
        }
        
        difference() {
            union() {
                cylinder(d=rim_id, h=face_thickness);
                
                mirror([0,0,1]) support();
                translate([0, 0, face_thickness]) support();
            }
            
            for (a=[0 : 360/4 : 360]) {
                rotate([0, 0, a]) translate([0, (rim_id/4)*1.15]) cylinder(d=30, h=alot, center=true);
            }
        }
    }
    
    difference() {
        union() {
            outer();
            translate(offset_absolute) {
                face();
                cw01_rearwheelmount_add();
                cylinder(d1=30, d2=20, h=cw01_rearwheelmount_outside_h);
            }
        }
        
        cw01_rearwheelmount_sub();
    }
}
$fn=32; cw01_sandtire_rear(side="left");
//$fn=32; cw01_sandtire_rear(side="right");