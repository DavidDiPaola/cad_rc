use <lib/scad_misc/tube.scad>

module tool_brushlessrotortool() {
    tube_id = 12.75;
    tube_od = 14;
    tube_h  = 30;
    tube(id=tube_id, od=tube_od, h=tube_h);
    
    // TODO chamfer
    
    translate([0,0,tube_h]) tube(id=tube_id, od=tube_od+(5*2), h=2);
}
//$fn=32; tool_brushlessrotortool();  // for dev
$fn=32; mirror([0,0,1]) tool_brushlessrotortool();  // for printing
