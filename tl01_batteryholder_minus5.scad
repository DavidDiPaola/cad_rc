use <lib/tl01_batteryholder.scad>

/*
A modified replacement for Tamiya TL-01 part C10 (from Tamiya part no. 50737).

This part is designed (along with its plus5 counterpart) to shift a Li-Po round
battery 5mm from the center so that the battery leads do not push up against a body.
It also has more room for the battery leads to accomodate larger Li-Po wires and
balance cables.

Required hardware:
    - None

FDM printer settings:
    - Infill: 100%
    - Supports: Yes
    - Adhesion: Brim

Post-print:
    1. Drill out the post hole with a 5.5mm drill bit.
*/
module tl01_batteryholder_minus5(length_tweak=0, post_d_tweak=+0.5, thickness=2.5) {
    tl01_batteryholder(length_tweak=-5+length_tweak, post_d_tweak=post_d_tweak, thickness=thickness);
}
$fn=32; tl01_batteryholder_minus5();