/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

use <lib/wheel/dish_bearings.scad>

/*
outer beads = 30
bead = 4.5
*/

/*
HSP 94107 wheel based off measurements of a HSP 06010 tire.
*/
module wheel_dish_hsp06010_front_bearing(
    offset_=0,
    bearing_od=11+0.25, bearing_w=4, bearings_outer_thickness="default",
    bearings_separator_id=6, bearings_separator_w=2-0.25,
    
    thickness_default=1.5
) {
    rim_fin_thickness = thickness_default;
    
    wheel_dish_bearings(
        rim_outer_diam="2.2in", rim_outer_thickness="default", rim_inner_diam_minimum=52,
        rim_width=25+(rim_fin_thickness*2),
        rim_bead_width=4.0+0.5,
        rim_fin_height=1.5, rim_fin_thickness=rim_fin_thickness,
        rim_airhole_d=2,
        
        offset_=offset_,
        bearing_od=bearing_od, bearing_w=bearing_w, bearings_outer_thickness=bearings_outer_thickness,
        bearings_separator_id=bearings_separator_id, bearings_separator_w=bearings_separator_w,
        
        dish_thickness="default",
        spoke_thickness="default",
        
        thickness_default=thickness_default
    );
}
$fn=32; wheel_dish_hsp06010_front_bearing();