/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

use <lib/wheel/dish_hex.scad>

module wheel_dish_tamiya_holidaybuggy_rear_dt03() {
    wheel_dish_hex(
        rim_outer_diam="2.2in", rim_outer_thickness="default", rim_inner_diam_minimum=52 /*for DT-03*/,
        rim_width=40,
        rim_bead_width=5,
        rim_fin_height=2, rim_fin_thickness="default",
        rim_airhole_d=2,
    
        offset_=12.5 /*from DT-03 stock wheels (Astral Dish)*/,
        hex_width=6, hex_diam=12+0.5, hex_thickness="default",
        axle_d=4+0.5,
    
        dish_thickness="default",
        spoke_thickness="default",
        
        thickness_default=1.5
    );
}
$fn=32; wheel_dish_tamiya_holidaybuggy_rear_dt03();