# Tamiya Rising Fighter rear shock mount installation instructions

<img width="50%" src="img/installed.jpg" />


## Step 1: Preparation

Hardware needed:

  - 4x M3x8mm screws (my kit came with enough spares of these)

Tools needed:

  - hobby knife or rotary tool
  - 3mm or 7/32in drill bit

Print 2 rear shock mounts:

  - material: any?
  - infill: 100%
  - support: yes, touching build plate


## Step 2: Remove the rear wing


## Step 3: Unscrew the step screws that hold the top of the shocks onto the chassis

The shock spacers (part D4) will not be used anymore. Store them in a safe place in case you want to undo these modifications.


## Step 4: Trim the wing support bracket (part D3)

Trim the area outlined by the dotted red line:<br/>
<img width="50%" src="img/step04a.jpg" />

It should look something like this when you're done:<br/>
<img width="50%" src="img/step04b.jpg" />


## Step 5: Drill out lower screw hole

Align the bottom of the shock mount with the bottom of the wing support bracket:<br/>
<img width="50%" src="img/step05a.jpg" />

Use a 3mm or 7/64in drill bit to drill out the lower mounting hole:<br/>
<img width="50%" src="img/step05b.jpg" />


## Step 6: Install screws

<img width="50%" src="img/step06.jpg" />


## Step 7: Repeat steps 3 through 6 for the other side of the chassis


## Step 8: Re-install the rear wing

<img width="50%" src="img/step08.jpg" />


