use <lib/scad_misc/cube2.scad>

/* A dust cover for the Tamiya TL-01 chassis that covers the mechanical speed controller resistor hole.

Parameters:
    thickness -- the thickness of the part
    upper_x_tweak -- tweaks the width of the upper rectangle
    upper_y_tweak -- tweaks the height of the upper rectangle
    lower_x_tweak -- tweaks the width of the lower rectangle
    lower_y_tweak -- tweaks the height of the lower rectangle

Requred hardware:
    - 2x M3x8mm screws

Printing:
    - No special considerations.
    - Post-print:
        - Drill out screw holes with a 3.2mm or 1/8in drill bit.
*/
module tl01_dustcover(
    thickness=2,
    upper_x_tweak=-2.0, upper_y_tweak=-1.0,
    lower_x_tweak=-1.0, lower_y_tweak=0
) {
    upper = [ 40.0+upper_x_tweak, 19.5+upper_y_tweak, thickness ];
    lower = [ 28.0+lower_x_tweak, 12.0+lower_y_tweak, thickness ];
    
    module hole() {
        translate([10, 0]) cylinder(d=3.2, h=thickness);
    }
    
    difference() {
        union() {
            cube2(lower, center_x=true);
            translate([0, lower.y]) cube2(upper, center_x=true);
        }
        
        translate([0, lower.y-4.5]) {
            hole();
            mirror([1,0]) hole();
        }
    }
}
tl01_dustcover($fn=32);