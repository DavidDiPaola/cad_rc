/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

use <lib/wheel/dish_hex.scad>

/*
HSP 94107 wheel based off measurements of a HSP 06010 tire.
*/
module wheel_dish_hsp06010_front() {
    rim_fin_thickness = 1;
    
    wheel_dish_hex(
        rim_outer_diam="2.2in", rim_outer_thickness=1.5, rim_inner_diam_minimum=52,
        rim_width=28+(rim_fin_thickness*2),
        rim_bead_width=4.0+0.5,
        rim_fin_height=1.5, rim_fin_thickness=rim_fin_thickness,
        rim_airhole_d=2,
    
	offset_=5,
        hex_width=4, hex_diam=12+0.5, hex_thickness=1.5,
        axle_d=4.2,
    
        dish_thickness=1.5,
        spoke_thickness=1.5
    );
}
$fn=32; wheel_dish_hsp06010_front();
