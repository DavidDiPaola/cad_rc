/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

use <lib/shock/clip.scad>

module shock_clip_tamiyacva(height=4) {
    shock_clip(shock_diam=12, height=height, thickness=2);
}

$fn=32;
shock_clip_tamiyacva();
