use <lib/bodypost_post.scad>

use <lib/scad_misc/cube2.scad>

use <lib/scad_misc/mscrew.scad>

/*
A bodypost for 3Racing Sakura chassis.

FDM slicer settings:
    - Infill: 100%
    - Supports: yes

Post-print:
    1. Drill bottom screw hole with a 2.5mm drill bit.
*/
module sakura_bodypost_front(square_x_tweak=0, square_z_tweak=0, hole_d_tweak=0) {
    bottom_x = 10;
    bottom_z = 11;
    
    square_x = 5.6 + square_x_tweak;
    square_z = 2.0 + square_z_tweak;
    
    post_d = 5.8;

    difference() {
        union() {
            translate([0, 0, bottom_z]) bodypost_post(
                post_d=post_d, post_h=68,
                hole_d=2+hole_d_tweak, hole_space=3.8
            );
        
            cube2([bottom_x, 1.5, bottom_z], center_x=true, center_y=true);
            cylinder(d=post_d, h=bottom_z);
            cylinder(d=6.7, h=8.5);
            cylinder(d=bottom_x, h=2);
            mirror([0,0,1]) cube2([square_x, square_x, square_z], center_x=true, center_y=true);
        }
        
        translate([0, 0, -square_z]) mscrew_threadhole_sub(m=3, l=bottom_z, thread_type="machine");
    }
        
}
rotate([0, 90]) sakura_bodypost_front($fn=32);