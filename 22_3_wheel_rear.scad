/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

use <lib/wheel/dish_hex.scad>

/*
Losi 22 3.0 rear wheel.
*/
module 22_3_wheel_rear(offset_=8) {
    /* outer diam. measured as ~55.25mm */
    /* inner diam. measured as ~53mm*/
    /* offset measured as 8mm = 27mm - (38mm / 2) */
    /* inner fin thickness measured as 1.25mm */
    
    rim_width = 38;
    fin_thickness = 1;
    
    wheel_dish_hex(
        rim_outer_diam="2.2in", rim_outer_thickness="default", rim_inner_diam_minimum=52,
        rim_width=38,
        rim_bead_width=6*1.05,
        rim_fin_height=2.5, rim_fin_thickness=fin_thickness,
        rim_airhole_d=2,
    
        offset_=offset_,
        hex_width=4, hex_diam=12*1.05, hex_thickness="default",
        axle_d=5*1.125,
    
        dish_thickness="default",
        spoke_thickness="default",
        
        thickness_default=1.5
    );
}
$fn=32; 22_3_wheel_rear();