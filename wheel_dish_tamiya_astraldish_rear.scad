/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

use <lib/wheel/dish_hex.scad>

module wheel_dish_tamiya_astraldish_rear() {
    wheel_dish_hex(
        rim_outer_diam=54, rim_outer_thickness=1, rim_inner_diam_minimum=52,
        rim_width=35,
        rim_bead_width=6,
        rim_fin_height=3, rim_fin_thickness="default",
        rim_airhole_d=false,
    
        offset_=12.5,
        hex_width=4, hex_diam=12+0.5, hex_thickness="default",
        axle_d=4+0.5,
    
        dish_thickness="default",
        spoke_thickness="default",
        
        thickness_default=1.5
    );
}
$fn=32; wheel_dish_tamiya_astraldish_rear();