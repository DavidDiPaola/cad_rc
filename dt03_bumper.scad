/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

include <lib/scad_misc/alot.scad>

use <lib/scad_misc/cube2.scad>

use <lib/scad_misc/mscrew.scad>

use <lib/scad_misc/wedge.scad>

module dt03_bumper(bumper_width=120, bumper_height=20, bumper_angle=20) {
    plate = [36, 57, 3.5];
    
    module plate_() {
        screwholes = [ 19.5, 20.5 ];
        module screwhole() {
            mscrew_well_sub(m=3, l=alot, thread_type="hole", align_welltop=true);;
        }        
        module screwhole_row() {
            translate([-screwholes.x/2, 0]) screwhole();
            translate([+screwholes.x/2, 0]) screwhole();
        }
        
        plate_offset = [0, 15];
        
        difference() {
            union() {
                cube2(plate, center_x=true);
                
                translate([0, plate_offset.y+screwholes.y+15, plate.z]) {
                    mirror([0,1]) cube2([8, 3, 8], center_x=true);
                    translate([0, 3.25]) cube2([8, 3, 8], center_x=true);
                }
            }
            
            translate([0, plate_offset.y]) {
                screwhole_row();
                translate([0, screwholes.y]) screwhole_row();
            }
        }
        
    }
    //plate_();
    
    module plate() {
        screwholes = [ 19.5, 20.5 ];
        module screwhole() {
            mscrew_well_sub(m=3, l=alot, thread_type="hole", align_welltop=true);;
        }        
        module screwhole_row() {
            translate([-screwholes.x/2, 0]) screwhole();
            translate([+screwholes.x/2, 0]) screwhole();
        }
        
        module pinholder(gap=3.25) {
            block_size = [8, 3, 8];
            module block() {
                cube2(block_size, center_x=true);
            }
            
            translate([0, -gap/2]) {
                mirror([0,1]) block();
                translate([0, -block_size.y]) mirror([0,1]) wedge(l=4, w=block_size.x, h=block_size.z, center_x=true);
            }
            translate([0, +gap/2]) block();
        }
        //pinholder();
        
        plate_offset = [0, 15];
        
        module half() {
            difference() {
                union() {
                    cube([plate.x/2, plate.y, plate.z]);
                    
                    translate([0, plate_offset.y+screwholes.y+17, plate.z]) pinholder();
                }
                
                translate([0, plate_offset.y]) {
                    screwhole_row();
                    translate([0, screwholes.y]) screwhole_row();
                }
            }
        }
        
        half();
        mirror([1,0]) half();
        
    }
    plate();
    
    module bumper() {
        bumper = [bumper_width, bumper_height, 3.5];
        
        minkowski_r = 5;
        minkowski_h = 1;
        
        module half() {
            translate([0, minkowski_r]) {
                difference() {
                    cube([(bumper.x/2)-minkowski_r, bumper.y-(minkowski_r*2), bumper.z-minkowski_h]);
                
                    translate([plate.x/2, 0]) rotate([0, 0, 10]) mirror([0, 1]) cube(alot_3d);
                }
            }
            
            
        };
        
        minkowski() {
            union() {
                half();
                mirror([1,0]) half();
            }
            
            cylinder(r=minkowski_r, h=minkowski_h);
        }
    }
    //bumper();
    
    difference() {
        union() {
            plate();
            translate([0, plate.y]) {
                rotate([bumper_angle, 0]) bumper();
                translate([0, 0, plate.z]) wedge(l=20, w=8, h=8, center_x=true);
            }
        }
        
        translate([0, plate.y]) rotate([bumper_angle, 0]) mirror([0,0,1]) cube2(alot_3d, center_x=true);
    }
}
$fn=32; dt03_bumper();