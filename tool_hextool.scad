use <../scad_misc/hexagon.scad>

/*
Allows a hex wheel to be spun by an electric drill.
Print settings:
    infill: 100%
    support: yes (touching build plate)
Hardware needed:
    M4x10 screw (or shorter)
*/
module tool_hextool(hex_d=12) {
    hole_d = 4;
    
    difference() {
        union() {
            hexagon(d=hex_d, h=6);
            hexagon(d=hole_d+(1.5*2), h=15);
        }
        
        cylinder(d=hole_d-0.1, h=10);
    }
}
$fn=32; mirror([0,0,1]) tool_hextool();
