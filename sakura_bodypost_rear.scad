use <lib/bodypost_post.scad>

use <lib/scad_misc/cube2.scad>

use <lib/scad_misc/wedge.scad>

use <lib/scad_misc/mscrew.scad>

/*
A bodypost for 3Racing Sakura chassis.

FDM slicer settings:
    - Infill: 100%
    - Supports: yes

Post-print:
    1. Drill bottom screw hole with a 2.5mm drill bit.
*/
module sakura_bodypost_rear(hole_d_tweak=0) {
    post_d = 5.8;
    
    bottom = [ 7.5, 11, 14 ];
    bottom_xlate = [ 0, -post_d/2, 0 ];
    bottom_y_max = bottom.y + bottom_xlate.y;

    difference() {
        union() {
            bodypost_post(
                post_d=post_d, post_h=68,
                hole_d=2+hole_d_tweak, hole_space=3.8
            );
        
            translate(bottom_xlate) mirror([0,0,1]) cube2([bottom.x, bottom.y, bottom.z], center_x=true);
            translate([0, bottom_y_max, -10.5]) rotate([-90, 0]) cylinder(d=2.7, h=3);
        }

        translate([0, 5, -bottom.z]) rotate([-270+45, 0]) cube2(bottom, center_x=true);
        
        translate([0, bottom_y_max, -3.0]) rotate([90, 0]) mscrew_threadhole_sub(m=3, l=bottom.y, thread_type="machine");
    }
        
}
rotate([0, 90]) sakura_bodypost_rear($fn=32);