# TODO use OPENSCADPATH ?

SCAD = \
	22_3_wheel_rear.scad \
	cw01_sandtire_rear.scad \
	risingfighter_headlamp.scad \
	risingfighter_rearshockmount.scad \
	shock_clip_tamiyacva.scad \
	sakura_bodypost_front.scad \
	tl01_arm_upper_front.scad \
	tl01_arm_upper_rear.scad \
	tl01_batteryholder_minus5.scad \
	tl01_batteryholder_plus5.scad \
	tl01_bodymount_front.stl \
	tl01_bodymount_rear.stl \
	tl01_dustcover.scad \
	tl01_gearbox_cover.scad \
	tl01_shockmount_front.scad \
	tl01_shockmount_rear.scad \
	tool_brushlessrotortool.scad \
	tool_hextool.scad \
	wheel_dish_hsp06010_front.scad \
	wheel_dish_hsp06010_front_bearing.scad \
	wheel_dish_hsp06026_rear.scad \
	wheel_dish_tamiya_astraldish_rear.scad \
	wheel_dish_tamiya_holidaybuggy_rear_dt03.scad
STL = $(SCAD:.scad=.stl)
MD = \
	doc/risingfighter/rearshockmount/index.md
HTML = $(MD:.md=.html)
ZIP = release.zip

.PHONY: all
all: $(STL) doc

.PHONY: doc
doc: $(HTML)

.PHONY: clean
clean:
	rm -rf $(STL)
	rm -rf $(HTML)
	rm -rf $(ZIP)

.PHONY: dist
dist: $(ZIP)

%.stl: %.scad
	openscad -o $@ $<

%.html: %.md
	markdown -o $@ $<

$(ZIP): $(STL) $(HTML)
	rm -rf zip
	mkdir -p zip/CAD_RC/
	cp $(STL) zip/CAD_RC/
	cp -r doc/ zip/CAD_RC/
	cd zip; zip -r ../$@ ./CAD_RC/
	rm -rf zip

