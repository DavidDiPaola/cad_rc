include <lib/scad_misc/alot.scad>

/*
A replacement for the Tamiya TL-01 part B3 (from Tamiya part no. 50736).

Required hardware:
    - 1x M3x8mm (or 10mm) screw

FDM printer settings:
    - Infill: 100%
    - Supports: Yes

Post-print:
    1. Drill out the screw hole with a 3.2mm or 1/8in drill bit.
*/
module tl01_gearbox_cover(nub_d_tweak=+0.5, nub_h_tweak=+0.5, cap_d_tweak=-0.7) {
    nub_d = 7.0 + nub_d_tweak;
    nub_h = 5.9 + nub_h_tweak;
    module nubcap_add() {
        cylinder(d=nub_d+2, h=nub_h+1.5);
    }
    module nubcap_sub() {
        cylinder(d=nub_d, h=nub_h);
        
        cylinder(d=3.2, h=alot);
    }
    
    module cap() {
        mirror([0,0,1]) cylinder(d=12.2+cap_d_tweak, h=2);
        cylinder(d=14.3, h=2);
    }
    
    difference() {
        union() {
            nubcap_add();
            
            translate([9.5, 0]) cap();
        }
        
        nubcap_sub();
    }
}
$fn=32; mirror([0,0,1]) tl01_gearbox_cover();