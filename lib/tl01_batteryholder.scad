use <scad_misc/wedge.scad>

use <scad_misc/cube2.scad>

/*
A replacement for the Tamiya TL-01 part XX (from Tamiya part no. XXXXX).

Required hardware:
    - None

FDM printer settings:
    - Infill: 100%
    - Supports: Yes
    - Adhesion: Brim

Post-print:
    1. Drill out the post hole with a 5mm drill bit.
*/
module tl01_batteryholder(length_tweak=+5, post_d_tweak=+0.5, thickness=2.5) {
    width = 26;
    post_d = 5 + post_d_tweak;
    post_support_d = 12;
    length = post_support_d + length_tweak;
    top = [
        thickness,
        post_support_d + post_d + length,
        width
    ];
    
    difference() {
        cube(top);
    
        translate([0, post_support_d + (post_d/2), top.z/2]) rotate([0, 90]) cylinder(d=5, h=thickness);
    }
    
    cutout = [17, thickness, 15];
    translate([0, top.y]) {
        difference() {
            union() {
                mirror([0,1]) cube([width, thickness, width]);
            
                translate([+thickness, -thickness]) rotate([180, -90]) wedge(l=2, w=width, h=2);
            }
            
            translate([width, 0, width/2]) rotate([0, 0, 180]) cube2(cutout, center_z=true);
        }
    }
}
$fn=32; tl01_batteryholder();