/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

/* Usage: `include <cw01_rearwheelmount.scad>` */

use <scad_misc/tube.scad>

include <scad_misc/alot.scad>

cw01_rearwheelmount_h = 13;
cw01_rearwheelmount_inside_h = 3;
cw01_rearwheelmount_outside_h = cw01_rearwheelmount_h - cw01_rearwheelmount_inside_h;
cw01_rearwheelmount_od = 30;

/*
Additive parts of the CW-01 rear wheel mount.
    axle_id_twk -- axle hole outer diameter tweak
    nub_d_twk -- nub diameter tweak
*/
module cw01_rearwheelmount_add(axle_od_twk=-0.2, nub_d_twk=0) {
    mirror([0,0,1]) {
        for (a=[0 : 360/5 : 360]) {
            rotate([0, 0, a]) translate([0, 10]) cylinder(d=5.8+nub_d_twk, h=cw01_rearwheelmount_inside_h);
        }
        
        translate([0, 0, -cw01_rearwheelmount_outside_h]) cylinder(d=8.7+axle_od_twk, h=cw01_rearwheelmount_h);
    }
}

/*
Subtractive parts of the CW-01 rear wheel mount.
    axle_id_twk -- axle hole inner diameter tweak
*/
module cw01_rearwheelmount_sub(axle_id_twk=+0.5) {
    cylinder(d=5+axle_id_twk, h=alot, center=true);
}

module cw01_rearwheelmount_test() {
    difference() {
        union() {
            cw01_rearwheelmount_add();
            
            cylinder(d=cw01_rearwheelmount_od, h=2);
        }
        
        cw01_rearwheelmount_sub();
    }
}
//$fn=32; cw01_rearwheelmount_test();