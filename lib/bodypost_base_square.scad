include <scad_misc/alot.scad>

use <scad_misc/cube2.scad>

use <scad_misc/cylinder_bent.scad>

use <scad_misc/mscrew.scad>

use <bodypost_post.scad>

module bodypost_post_add(d=5, h=30) {
    cylinder(d=d, h=h);
    
    translate([0, 0, h]) sphere(d=d);
}

module bodypost_post_sub(
    d=5, h=30,
    hole_d=1, hole_space=4
) {
    for(z=[1 : (h/hole_space)]) {
        translate([0, 0, z*hole_space]) rotate([0, 90]) cylinder(d=hole_d, h=d, center=true);
    }
}
//$fn=32; difference() { bodypost_post_add(); bodypost_post_sub(); }

function bodypost_base_h(
    square_depth,
    post_d,
    thickness
) =
    square_depth
    +
    max(
        post_d/2,
        thickness
    )
;

function bodypost_post_xlate(post_offset_x, post_offset_y, square_width) = [
    post_offset_x,
    post_offset_y,
    square_width / 2
];

module bodypost_base_square_add(
    square_width, square_depth, base_d,
    post_offset_x, post_offset_y, post_d, post_h, post_hole_d, post_hole_space,
    thickness
) {
    base_r = base_d / 2;
    base_h = bodypost_base_h(
        square_depth=square_depth,
        post_d=post_d,
        thickness=thickness
    );
    
    rotate([0, -90]) translate([0, 0, -square_depth]) {
        cylinder(d=base_d, h=base_h);
    }
    
    bodypost_post(
        post_d=post_d, post_h=post_h,
        hole_d=post_hole_d, hole_space=post_hole_space,
        offset_x=post_offset_x, offset_y=post_offset_y,
        bend_r=5
    );
}

module bodypost_base_square_sub(
    square_width, square_depth, base_d,
    post_offset_x, post_offset_y, post_d, post_h, post_hole_d, post_hole_space,
    thickness
) {
    base_h = bodypost_base_h(
        square_depth=square_depth,
        post_d=post_d,
        thickness=thickness
    );
    rotate([0, -90]) {
        translate([0, 0, -square_depth]) {
            mirror([0,0,1]) cylinder(d=base_d, h=alot);
            
            cube2([square_width, square_width, square_depth], center_x=true, center_y=true);
            
            cylinder(d=3.2, h=alot);
            translate([0, 0, base_h]) cylinder(d=5, h=alot);
        }
    }
}

module bodypost_base_square(
    square_width=5.75, square_depth=3.5, base_d=10,
    post_offset_x=0, post_offset_y=0, post_d=5, post_h=30, post_hole_d=1, post_hole_space=4,
    thickness=1.5
) {
    difference() {
        bodypost_base_square_add(
            square_width=square_width, square_depth=square_depth, base_d=base_d,
            post_offset_x=post_offset_x, post_offset_y=post_offset_y, post_d=post_d, post_h=post_h, post_hole_d=post_hole_d, post_hole_space=post_hole_space,
            thickness=thickness
        );
        
        bodypost_base_square_sub(
            square_width=square_width, square_depth=square_depth, base_d=base_d,
            post_offset_x=post_offset_x, post_offset_y=post_offset_y, post_d=post_d, post_h=post_h, post_hole_d=post_hole_d, post_hole_space=post_hole_space,
            thickness=thickness
        );
    }
}
//bodypost_base_square($fn=32);
//bodypost_base_square(square_width=5.75+0.5, post_offset_y=-18, $fn=32);
//bodypost_base_square(square_width=5.75+0.5, post_offset_x=-10, post_offset_y=-18, $fn=32);