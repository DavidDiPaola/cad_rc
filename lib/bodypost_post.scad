use <scad_misc/cylinder_bent.scad>

function bodypost_do_bend(
    offset_y,
    bend_r
) =
    abs(offset_y) > bend_r
;

function bodypost_post_offset(
    offset_x, offset_y,
    bend_r
) =
    [
        offset_x,
        offset_y,
        bodypost_do_bend(offset_y=offset_y, bend_r=bend_r) ? bend_r : 0
    ]
;

module bodypost_post_add(
    post_d, post_h,
    hole_d, hole_space,
    offset_x, offset_y,
    bend_r,
) {
    post_offset = bodypost_post_offset(
        bend_r=bend_r,
        offset_x=offset_x, offset_y=offset_y
    );
    do_bend = bodypost_do_bend(offset_y=post_offset.y, bend_r=bend_r);
    
    module base() {
        off_x = abs(post_offset.x);
        off_y = abs(post_offset.y);
        
        base_angle = atan(off_x / off_y) * (post_offset.x < 0 ? -1 : 1);
        base_length = sqrt(pow(off_x, 2) + pow(off_y, 2));
        rotate([0, 0, base_angle]) {
            rotate([90, 0]) {
                sphere(d=post_d);
                cylinder(d=post_d, h=base_length-bend_r);
            }
            
            translate([
                0,
                -base_length + bend_r,
                bend_r
            ])
            rotate([0, 90, 180]) {
                cylinder_bent(d=post_d, angle=90, bend_r=bend_r);
            }
        }
    }

    if (do_bend) {
       mirror([
           0,
           post_offset.y < 0 ? 0 : 1
       ]) base();
    }

    post_base_h = post_h - (post_d/2);        
    translate(post_offset) {
        cylinder(d=post_d, h=post_base_h);
        translate([0, 0, post_base_h]) sphere(d=post_d);
    }
}

module bodypost_post_sub(
    post_d, post_h,
    hole_d, hole_space,
    offset_x, offset_y,
    bend_r
) {
    post_offset = bodypost_post_offset(
        bend_r=bend_r,
        offset_x=offset_x, offset_y=offset_y
    );
    translate(post_offset) {
        for(z=[1 : (post_h/hole_space)]) {
            translate([0, 0, z*hole_space]) rotate([0, 90]) cylinder(d=hole_d, h=post_d, center=true);
        }
    }
}

module bodypost_post(
    post_d=6, post_h=30,
    hole_d=1, hole_space=4,
    offset_x=0, offset_y=0,
    bend_r=5
) {
    difference() {
        bodypost_post_add(
            post_d=post_d, post_h=post_h,
            hole_d=hole_d, hole_space=hole_space,
            offset_x=offset_x, offset_y=offset_y,
            bend_r=bend_r
        );
        
        bodypost_post_sub(
            post_d=post_d, post_h=post_h,
            hole_d=hole_d, hole_space=hole_space,
            offset_x=offset_x, offset_y=offset_y,
            bend_r=bend_r
        );
    }
}

//bodypost_post($fn=32);
//bodypost_post(offset_y=-20, $fn=32);
//bodypost_post(offset_y=+20, $fn=32);
//bodypost_post(offset_x=10, offset_y=-20, $fn=32);
//bodypost_post(offset_x=-10, offset_y=-20, $fn=32);
//bodypost_post(offset_x=10, offset_y=20, $fn=32);