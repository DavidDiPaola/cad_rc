/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

// usage: `use <lib/headlamp.scad>`

include <scad_misc/alot.scad>

/*
A headlamp.
    lamp_d -- the diameter of the face
    face_h -- the height of the non-spherical part of the face
    post_hole_d -- the diameter of the hole in the post
    post_thickness -- the thickness of the walls of the post
    post_h -- the height of the post beyond the edge of the face
    post_end_angle_x,post_end_angle_y -- the angle that the post terminates at
*/
module headlamp(
    lamp_d=15,
    face_h=2,
    post_hole_d=3, post_thickness=1.5, post_h=3, post_end_angle_x=0, post_end_angle_y=0
) {
    sphere_y = -face_h;
    post_y = sphere_y + (-lamp_d/8);
    post_h_total = (lamp_d/2)+post_h;
    post_d = post_hole_d + (post_thickness*2);
    
    module cube_alot_center_xy() {
        translate([-alot/2, 0, -alot/2]) cube([alot,alot,alot]);
    }
    
    difference() {
        union() {
            translate([0,sphere_y]) {
                sphere(d=lamp_d);
            
                rotate([-90,0,0]) cylinder(d=lamp_d, h=face_h);
            }
                
            translate([0, post_y]) mirror([0,0,1]) cylinder(d=post_d, h=alot);
        }
        
        cube_alot_center_xy();
        
        translate([0, post_y]) mirror([0,0,1]) cylinder(d=post_hole_d, h=alot);
        
        translate([0, post_y, -post_h_total]) mirror([0,0,1]) rotate([post_end_angle_x+90,-post_end_angle_y,0]) cube_alot_center_xy();
    }
}
//$fn = 32; headlamp();
