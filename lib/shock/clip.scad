/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

// usage: `use <lib/shock/clip.scad>`

include <../scad_misc/alot.scad>

/*
A shock pre-load clip.
	shock_diam -- the diameter of the shock this will be clipped to
	height -- the height of this clip
	thickness -- the thickness of this clip (x-axis)
*/
module shock_clip(shock_diam=12, height=4, thickness=2) {
    innerdiam = shock_diam;
    outerdiam = innerdiam + (thickness*2);
    
    module tab() {
        tab_x = (thickness*2);
        translate([-tab_x/2,0]) {
            difference() {
                union() {
                    cube([tab_x, thickness, height]);
                    translate([tab_x/2,thickness]) cylinder(d=tab_x, h=height);
                }
                
                mirror([0,1,0]) cube([alot,alot,alot]);
            }
        }
    } //tab();
    
    difference() {
        union() {
            cylinder(d=outerdiam, h=height);
            
            translate([0,innerdiam/2]) tab();
        }
        
        cylinder(d=innerdiam, h=height);
        
        cutout_x = shock_diam * 0.75;
        mirror([0,1,0]) translate([-cutout_x/2,0]) cube([cutout_x, alot, height]);
    }
}
//$fn=32; shock_clip();

