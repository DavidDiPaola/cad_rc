/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

use <../../scad_misc/valueordefault.scad>

function wheel_bearings_d(bearing_od, outer_thickness, thickness_default=1.5) =
    bearing_od
    +
    (valueordefault(outer_thickness, thickness_default)*2)
;
function wheel_bearings_h(bearing_w, separator_w) =
    (bearing_w*2) + separator_w
;
function wheel_bearings_xlate(offset_) =
    [0, 0, -(offset_)]
;
function wheel_bearings_xlate_bottom(offset_, bearing_w, separator_w) =
    wheel_bearings_xlate(offset_=offset_)
    -
    [0, 0, (wheel_bearings_h(bearing_w=bearing_w, separator_w=separator_w)/2)]
;
function wheel_bearings_xlate_top(offset_, bearing_w, separator_w) =
    wheel_bearings_xlate_bottom(offset_=offset_, bearing_w=bearing_w, separator_w=separator_w)
    +
    [0, 0, wheel_bearings_h(bearing_w=bearing_w, separator_w=separator_w)]
;

/*

    thickness_default -- the default value for thickness
*/
module wheel_bearings_add(
    bearing_od=11, bearing_w=4, outer_thickness="default",
    separator_w=1,
    offset_=0,
    
    thickness_default=1.5
) {
    _outer_thickness = valueordefault(outer_thickness, thickness_default);
      
    center_d = wheel_bearings_d(bearing_od=bearing_od, outer_thickness=_outer_thickness);
    center_h = wheel_bearings_h(bearing_w=bearing_w, separator_w=separator_w);
    translate(wheel_bearings_xlate(offset_=offset_)) {
        translate(wheel_bearings_xlate_bottom(offset_=0, bearing_w=bearing_w, separator_w=separator_w)) {
            cylinder(d=center_d, h=center_h);
        }
    }
}
//$fn=32; wheel_bearings_add();

/*
*/
module wheel_bearings_sub(
    bearing_od=11, bearing_w=4,
    separator_id=9, separator_w=1,
    offset_=0,
) {
    module half() {
        separator_w_half = separator_w/2;
        translate([0, 0, separator_w_half]) {
            cylinder(d=bearing_od, h=bearing_w);
        }
        
        cylinder(d=separator_id, h=bearing_w+separator_w_half);
    }
    translate(wheel_bearings_xlate(offset_=offset_)) {
        half();
        mirror([0, 0, 1]) half();
    }
}
//$fn=32; wheel_bearings_sub();

/*

    thickness_default -- the default value for thickness
*/
module wheel_bearings(
    bearing_od=11, bearing_w=4, outer_thickness="default",
    separator_id=9, separator_w=1,
    offset_=0,
    
    thickness_default=1.5
) {
    difference() {
        wheel_bearings_add(
            bearing_od=bearing_od, bearing_w=bearing_w, outer_thickness="default",
            separator_w=separator_w,
            offset_=offset_,
            
            thickness_default=thickness_default
        );
        
        wheel_bearings_sub(
            bearing_od=bearing_od, bearing_w=bearing_w,
            separator_id=separator_id, separator_w=separator_w,
            offset_=offset_
        );
    }
}
//$fn=32; wheel_bearings();
//$fn=32; wheel_bearings(offset_=6);