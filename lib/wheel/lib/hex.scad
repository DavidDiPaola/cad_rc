/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

include <../../scad_misc/alot.scad>

use <../../scad_misc/valueordefault.scad>

use <../../scad_misc/cube2.scad>

use <../../scad_misc/hexagon.scad>

use <rim.scad>

function wheel_hex_d(hex_diam, hex_thickness, thickness_default=1.5) =
    (hex_diam*1.2) + (valueordefault(hex_thickness, thickness_default)*2)
;
function wheel_hex_h(hex_width, hex_face_thickness, thickness_default=1.5) =
    hex_width + valueordefault(hex_face_thickness, thickness_default)
;
function wheel_hex_xlate(offset_) =
    [0, 0, -(offset_)]
;
function wheel_hex_xlate_bottom(offset_, hex_face_thickness, thickness_default=1.5) =
    wheel_hex_xlate(offset_=offset_) + [0, 0, -valueordefault(hex_face_thickness, thickness_default)]
;
function wheel_hex_xlate_top(offset_, hex_width) =
    wheel_hex_xlate(offset_=offset_) + [0, 0, hex_width]
;

/*
The hex
    hex_width -- the width of the axle hex
    hex_diam -- the size of the axle hex (usually 12mm or 17mm)
    hex_thickness -- the amount of extra material around the hex hole to reinforce it (can also be "default")
    hex_face_thickness -- the amount of extra material below the hex hole for the face of the wheel (can also be "default")
    axle_d -- the diameter of the axle hole

    thickness_default -- the default value for thickness
*/
module wheel_hex_add(
    hex_width=6, hex_diam=12+0.5, hex_thickness="default", hex_face_thickness="default",
    offset_=0,
    
    thickness_default=1.5
) {
    _hex_thickness      = valueordefault(hex_thickness,      thickness_default);
    _hex_face_thickness = valueordefault(hex_face_thickness, thickness_default);
      
    center_d = wheel_hex_d(hex_diam, _hex_thickness);
    center_h = wheel_hex_h(hex_width, _hex_face_thickness);
    translate(wheel_hex_xlate(offset_=offset_)) {
        translate(wheel_hex_xlate_bottom(offset_=0, hex_face_thickness=_hex_face_thickness)) {
            cylinder(d=center_d, h=center_h);
        }
    }
}

/*
The hex
    hex_width -- the width of the axle hex
    hex_diam -- the size of the axle hex (usually 12mm or 17mm)
    hex_thickness -- the amount of extra material around the hex hole to reinforce it (can also be "default")
    hex_face_thickness -- the amount of extra material below the hex hole for the face of the wheel (can also be "default")
    axle_d -- the diameter of the axle hole

    thickness_default -- the default value for thickness
*/
module wheel_hex_sub(
    hex_width=6, hex_diam=12+0.5,
    offset_=0,
    axle_d=4
) {
    translate(wheel_hex_xlate(offset_=offset_)) {
        hexagon(d=hex_diam, h=hex_width);
                        
        cylinder(d=axle_d, h=alot, center=true);
    }
}

/*
The hex
    hex_width -- the width of the axle hex
    hex_diam -- the size of the axle hex (usually 12mm or 17mm)
    hex_thickness -- the amount of extra material around the hex hole to reinforce it (can also be "default")
    hex_face_thickness -- the amount of extra material below the hex hole for the face of the wheel (can also be "default")
    axle_d -- the diameter of the axle hole

    thickness_default -- the default value for thickness
*/
module wheel_hex(
    hex_width=6, hex_diam=12+0.5, hex_thickness="default", hex_face_thickness="default",
    axle_d=4,
    offset_=0,
    
    thickness_default=1.5
) {
    difference() {
        wheel_hex_add(
            hex_width=hex_width, hex_diam=hex_diam, hex_thickness=hex_thickness, hex_face_thickness=hex_face_thickness,
            offset_=offset_,
            thickness_default=thickness_default
        );
        
        wheel_hex_sub(
            hex_width=hex_width, hex_diam=hex_diam,
            offset_=offset_,
            axle_d=axle_d
        );
    }
}
//$fn=32; wheel_hex();
//$fn=32; wheel_hex(offset_=6);