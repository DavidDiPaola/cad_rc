/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

use <../../scad_misc/valueordefault.scad>

include <../../scad_misc/alot.scad>

function wheel_rim_outer_diam(outer_diam) =
    (outer_diam == "2.2in" ) ? (2.20*25.4) : 
    (outer_diam == "1.9in" ) ? (1.90*25.4) :
    (outer_diam == "1.68in") ? (1.68*25.4) :
    (outer_diam == "1.55in") ? (1.55*25.4) :
    outer_diam
;
function wheel_rim_inner_diam(outer_diam, outer_thickness, thickness_default=1.5) =
    wheel_rim_outer_diam(outer_diam) - (valueordefault(outer_thickness, thickness_default)*2)
;
function wheel_rim_bottom_xlate(width) =
    [0, 0, -width/2]
;

/*
A configurable RC wheel rim.
    outer_diam -- the outer diameter of the main body of the wheel (sans fins)
    outer_thickness -- the thickness of the main body of the wheel (can also be "default")
    inner_diam_minimum -- if the inner diameter is below this value, an error will be thrown
    width -- the total width of the wheel
    bead_width -- the width of the tire's bead
    fin_height -- the height of the fins that keep the tire bead in place
    fin_thickness -- the thickness of the fins that keep the tire bead in place (can also be "default")
    airhole_d -- the size of the vent hole (may be 0 or false to not have any vent hole at all)
    airhole_angle -- the location of the vent hole
    thickness_default -- the default value for thickness
*/
module wheel_rim(
    outer_diam="2.2in", outer_thickness="default", inner_diam_minimum=false,
    width=40,
    bead_width=5,
    fin_height=2, fin_thickness="default",
    airhole_d=2, airhole_angle=0,
    thickness_default=1.5
) {
    _outer_diam = wheel_rim_outer_diam(outer_diam);
    _outer_thickness = valueordefault(outer_thickness, thickness_default);
    _fin_thickness = valueordefault(fin_thickness, thickness_default);
    
    inner_diam = wheel_rim_inner_diam(_outer_diam, _outer_thickness);
    if (inner_diam_minimum != false) {
        if (inner_diam < inner_diam_minimum) {
            echo("ERROR: Inner diameter is too small. Check outer_diam or outer_thickness parameters.");
            assert(false);
        }
    }
    
    module fin_set() {
        module fin() {
            cylinder(d=_outer_diam+(fin_height*2), h=_fin_thickness);
        }
        
        translate([0,0, (width/2)-_fin_thickness]) {
            fin();
            translate([0,0, -((_fin_thickness*2)+bead_width)]) {
                fin();
            }
        }
    }
    
    difference() {
        union() {
            cylinder(d=_outer_diam, h=width, center=true);
            
            fin_set();
            mirror([0,0,1]) fin_set();
        }
        
        cylinder(d=inner_diam, h=width, center=true);
        
        if ((airhole_d != false) && (airhole_d > 0)) {
            rotate([-90,0,-airhole_angle]) cylinder(d=airhole_d, h=alot);
        }
    }
}
//$fn=32; wheel_rim();
//$fn=32; wheel_rim(outer_diam="1.9in");
//$fn=32; wheel_rim(outer_diam="1.68in");
//$fn=32; wheel_rim(outer_diam="1.55in");
//$fn=32; wheel_rim(airhole_angle=90);