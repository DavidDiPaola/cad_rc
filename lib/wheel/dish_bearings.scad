/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

include <../scad_misc/alot.scad>

use <../scad_misc/valueordefault.scad>

use <../scad_misc/cube2.scad>

use <lib/rim.scad>

use <lib/bearings.scad>

/*
A configurable RC buggy "dish"-style wheel.
    TODO update this
    outer_diam -- the outer diameter of the main body of the wheel (sans fins)
    outer_thickness -- the thickness of the main body of the wheel (can also be "default")
    inner_diam_minimum -- if the inner diameter is below this value, an error will be thrown
    width -- the total width of the wheel
    offset_ -- the position of the outside face of the hex relative to the center of the wheel
        zero offset: outside face of the hex is exactly at the center of the wheel
        positive offset: outside face of the hex is closer to the street
        negative offset: outside face of the hex is closer to the chassis
    hex_width -- the width of the axle hex
    hex_diam -- the size of the axle hex (usually 12mm or 17mm)
    hex_thickness -- the amount of extra material around the hex hole to reinforce it (can also be "default")
    spoke_thickness -- the thickness of the inner support spokes (can also be "default")
    bead_width -- the width of the tire's bead
    axle_diam -- the diameter of the axle
    dish_thickness -- the thickness of the dish face (can also be "default")
    fin_height -- the height of the fins that keep the tire bead in place
    fin_thickness -- the thickness of the fins that keep the tire bead in place (can also be "default")
    airhole_d -- the size of the vent hole (may be 0 or false to not have any vent hole at all)
    thickness_default -- the default value for thickness
*/
module wheel_dish_bearings(
    rim_outer_diam="2.2in", rim_outer_thickness="default", rim_inner_diam_minimum=false,
    rim_width=30,
    rim_bead_width=5,
    rim_fin_height=2, rim_fin_thickness="default",
    rim_airhole_d=2, rim_airhole_angle=90,
    
    offset_=6,
    bearing_od=11+0.25, bearing_w=4, bearings_outer_thickness="default",
    bearings_separator_id=8, bearings_separator_w=2-0.25,
    
    dish_thickness="default",
    spoke_thickness="default",
    
    thickness_default=1.5
) {
    _dish_thickness = valueordefault(dish_thickness, thickness_default);
    _spoke_thickness = valueordefault(spoke_thickness, thickness_default);
    
    // rim calculations
    rim_inner_diam = wheel_rim_inner_diam(
        outer_diam=rim_outer_diam,
        outer_thickness=rim_outer_thickness,
        thickness_default=thickness_default
    );
    rim_xlate_bottom = wheel_rim_bottom_xlate(width=rim_width);
    
    // bearings calculations
    bearings_xlate = wheel_bearings_xlate(offset_=offset_);
    bearings_xlate_top = wheel_bearings_xlate_top(offset_=offset_, bearing_w=bearing_w, separator_w=bearings_separator_w);
    bearings_xlate_bottom = wheel_bearings_xlate_bottom(offset_=offset_, bearing_w=bearing_w, separator_w=bearings_separator_w);
    bearings_d = wheel_bearings_d(bearing_od=bearing_od, outer_thickness=bearings_outer_thickness, thickness_default=thickness_default);
    
    // dish calculations
    dish_face_d1 = rim_inner_diam;
    dish_face_d2 = bearings_d;
    dish_h = (bearings_xlate_bottom.z - rim_xlate_bottom.z);
    module dish_add() {
        translate(rim_xlate_bottom) {
            cylinder(d=dish_face_d1, h=_dish_thickness);
            translate([0, 0, _dish_thickness]) {
                cylinder(d1=dish_face_d1, d2=dish_face_d2, h=dish_h);
            }
        }
    }
    module dish_sub() {
        translate(rim_xlate_bottom) {
            cylinder(d1=dish_face_d1, d2=dish_face_d2, h=dish_h);
        }
    }
    
    // spoke calculations
    spoke_size = [
        _spoke_thickness,
        rim_inner_diam/2,
        bearings_xlate_top.z - rim_xlate_bottom.z
    ];
    module spokes() {
        translate(rim_xlate_bottom) {
            for (a=[0 : 360/6 : 360]) {
                rotate([0, 0, a]) cube2(spoke_size, center_x=true);
            }
        }
    }
    
    difference() {
        union() {
            wheel_rim(
                outer_diam=rim_outer_diam, outer_thickness=rim_outer_thickness, inner_diam_minimum=rim_inner_diam_minimum,
                width=rim_width,
                bead_width=rim_bead_width,
                fin_height=rim_fin_height, fin_thickness=rim_fin_thickness,
                airhole_d=rim_airhole_d, airhole_angle=rim_airhole_angle
            );
            
            wheel_bearings_add(
                bearing_od=bearing_od, bearing_w=bearing_w, outer_thickness=bearings_outer_thickness,
                separator_w=bearings_separator_w,
                offset_=offset_,
                thickness_default=thickness_default
            );

            dish_add();
            
            spokes();
        }
        
        wheel_bearings_sub(
            bearing_od=bearing_od, bearing_w=bearing_w,
            separator_id=bearings_separator_id, separator_w=bearings_separator_w,
            offset_=offset_
        );
        
        dish_sub();
    }
}
//$fn=32; wheel_dish_bearings();