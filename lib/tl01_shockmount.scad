use <scad_misc/arm.scad>

module tl01_shockmount(type="front", width=80, offset_z=2.5, thickness=4) {
    arm_d = 7.5;
    stock_x = (type == "front") ? 62 : 56;  // yes. the rear *is* narrower than the front.
    
    difference() {
        union() {
            arm_add(d=arm_d, l=stock_x, h=thickness, hole1_d=3, hole2_d=3, center_y=true);
            translate([offset_z, 0]) arm_add(d=arm_d, l=width, h=thickness, h1=7, hole1_d=3, h2=7, hole2_d=3, center_y=true);
        }
        
        arm_sub(d=arm_d, l=stock_x, h=thickness, hole1_d=3, hole2_d=3, center_y=true);
        translate([offset_z, 0]) arm_sub(d=arm_d, l=width, h=thickness, h1=7, hole1_d=3, h2=7, hole2_d=3, center_y=true);
    }
}
//$fn=32; tl01_shock_mount(type="front");
//$fn=32; tl01_shock_mount(type="rear");
