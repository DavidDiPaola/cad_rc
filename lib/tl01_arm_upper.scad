use <scad_misc/arm.scad>

module tl01_arm_upper(type="front", length_offset=0) {
    length = ((type == "front") ? 24 : 29) + length_offset;
    
    // hole1 diam. should really be ~3.8mm but I don't know of any common drill bits that size
    arm(d=8, l=length, h=8, hole1_d=4, hole2_d=2.5);
}
//$fn=32; tl01_arm_upper(type="front");
//$fn=32; tl01_arm_upper(type="rear");