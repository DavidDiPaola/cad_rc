use <lib/tl01_arm_upper.scad>

/*
A replacement for the Tamiya TL-01 part C3 (from Tamiya part no. 50737).
The arm length is estimated and may be incorrect.

Required hardware:
    - 2x 3x14mm Tamiya step screw

FDM printer settings:
    - Wall thickness: >=2mm

Post-print:
    1. Drill out the larger hole with a 4mm drill bit.
    2. Drill out the smaller hole with a 2.5mm drill bit.
*/
module tl01_arm_upper_front(length_offset=0) {
    tl01_arm_upper(type="front", length_offset=length_offset);
}
$fn=32; tl01_arm_upper_front();