/* 2020 David DiPaola. Licensed under CC0 (public domain, see https://creativecommons.org/publicdoma in/zero/1.0/). */

use <lib/headlamp.scad>

module risingfighter_headlamp(lamp_d=15, post_h=3) {
    headlamp(
        lamp_d=lamp_d,
        face_h=2,
        post_hole_d=2.8, post_h=3
    );
}
$fn=32; rotate([-90,0,0]) risingfighter_headlamp();
mirror([0,0,1]) cylinder(d=1, h=3);
